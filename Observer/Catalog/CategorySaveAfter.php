<?php


namespace M21\KategoriaGMC\Observer\Catalog;

class CategorySaveAfter implements \Magento\Framework\Event\ObserverInterface
{

    protected $__category;
    protected $kategoria_gmc;
    protected $storeId = 0;

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    )
    {

        $this->__category = $observer->getEvent()->getCategory();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $categoryResourceModel = $objectManager->create('Magento\Catalog\Model\ResourceModel\Category');
        $categoryFactory = $objectManager->create('Magento\Catalog\Model\CategoryFactory');

        $this->kategoria_gmc = $this->__category->getData('kategoria_gmc');
        $this->setChildCategoriesMarza($categoryResourceModel, $categoryFactory, $this->__category);

    }

    public function getChildCategories($category)
    {
        $collection = $category->getCollection()
            ->addIdFilter($category->getChildren());
        return $collection;
    }

    private function setChildCategoriesMarza($categoryResourceModel, $categoryFactory, $category)
    {

        if (strlen($this->kategoria_gmc)) {
            if ($chidlCtegoriesCollection = $this->getChildCategories($category)) {
                foreach ($chidlCtegoriesCollection as $subcategory) {
                    $_categoryFactory = $categoryFactory->create();
                    $categoryResourceModel->load($_categoryFactory, $subcategory->getId());
                    $_categoryFactory->setStoreId($this->storeId);
                    $_categoryFactory->setData('kategoria_gmc', $this->kategoria_gmc);
                    $categoryResourceModel->saveAttribute($_categoryFactory, 'kategoria_gmc');
                    // II lavel
                    $this->setChildCategoriesMarza($categoryResourceModel, $categoryFactory, $subcategory);
                }
            }
        }
    }
}